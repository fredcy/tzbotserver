import {
  DAppClient,
} from "@airgap/beacon-sdk";

// load libsodium explicitly to try to work around error:
//     sodium.randombytes_buf is not a function
const sodium = import('libsodium-wrappers');

console.log("register");

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function connectWallet(client) {
    let permissions = await client.requestPermissions();
    console.log('permissions', permissions);
}

async function main() {
    console.log("start");
    await sodium.ready;

    console.log("create client");
    const beaconClient = new DAppClient({
	name: "Tz Chat Registrar",
    });

    console.log("request permission");
    connectWallet(beaconClient)
	.catch(e => {
	    console.log('requestPermissions problem: ' + e.message);
	});
}

main();






